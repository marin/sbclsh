(in-package :sbcl-shell)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro with-lisp-string-name ((string lisp) thing &body body)
    (let ((g (gensym)))
      `(let* ((,g ,thing)
              (,string (cond ((and (listp ,g) (> (length ,g) 1))
                              (car ,g))
                             ((symbolp ,g)
                              (symbol-name ,g))
                             ((stringp ,g)
                              ,g)
                             (t (error "Unable to generate a name from ~A" ,g))))
              (,lisp (cond ((and (listp ,g) (> (length ,g) 1))
                            (cadr ,g))
                           ((symbolp ,g)
                            (symbol-name ,g))
                           ((stringp ,g)
                            (intern ,g))
                           (t (error "Unable to generate a name from ~A" ,g)))))
         ,@body)))
  (defun parse-defbuiltin-name (name)
    (if (atom name)
        (cond ((symbolp name)
               (list (symbol-name name) name))
              ((stringp name)
               (list name (intern name)))
              (t (error "Unable to generate a name from ~A" name)))
        name)))

(defun always-run-in-subshell-p (thing)
  (gethash thing *always-run-in-subshell*))

(defmacro defbuiltin (name args &body body)
  (let ((a (gensym)))
    (destructuring-bind (string-name lisp-name
                         &key read eval always-in-subshell-p
                         &allow-other-keys)
        (parse-defbuiltin-name name)
      `(progn
         ,(cond (eval
                 `(defun ,lisp-name (&rest ,a)
                    (apply (lambda ,args ,@body)
                           (mapcar (lambda (s) (eval (read-from-string s))) ,a))))
                (read
                 `(defun ,lisp-name (&rest ,a)
                    (apply (lambda ,args ,@body)
                           (mapcar (lambda (s) (read-from-string s)) ,a))))
                (t `(defun ,lisp-name ,args ,@body)))
         (setf (gethash ,string-name *builtin-hash*) #',lisp-name)
         ,@(when always-in-subshell-p
             `((setf (gethash ,string-name *always-run-in-subshell*) t
                     (gethash ',lisp-name   *always-run-in-subshell*) t)))))))

(defmacro defopts (&body options)
  `(unix-opts:make-options (list ,@(mapcar (lambda (d) (cons 'list d)) options))))

(defmacro with-options ((options-var freevars-var &key (argv '*argv*)
                                                    (options '*options*)
                                                    (skip-unknown-options t)
                                                    on-error-return-from
                                                    (getopt 'getopt))
                        &body body)
  (let* ((decls (if (and (listp (car body)) (eql (caar body) 'cl:declare))
                    (list (car body))
                    nil))
         (body (if decls
                   (cdr body)
                   body)))
    `(block ,on-error-return-from
       (multiple-value-bind (,options-var ,freevars-var)
           (handler-case
               (handler-bind ((unix-opts:unknown-option
                                (lambda (c)
                                  (format t "Warning: ~A is not a known option~%"
                                          (unix-opts:option c))
                                  (when ,skip-unknown-options
                                    (invoke-restart 'unix-opts:skip-option)))))
                 (unix-opts:get-opts ,argv ,options))
             (unix-opts:missing-arg (c)
               (dfmt t "Error: argument required for ~A~%"
                     (unix-opts:option c))
               (return-from ,on-error-return-from nil))
             (unix-opts:arg-parser-failed (c)
               (dfmt t "Error: unable to parse ~A for argument ~A"
                     (unix-opts:raw-arg c)
                     (unix-opts:option c))
               (return-from ,on-error-return-from nil))
             (unix-opts:missing-required-option (c)
               (dfmt t "Error: Missing required option(s): ~A" c)
               (return-from ,on-error-return-from nil)))
         ,@decls
         ,@(if getopt
               `((flet ((,getopt (option) (getf ,options-var option))) ,@body))
               body)))))

(defmacro defmain (name ((options free &key (skip-unknown-options t)
                                         (getopt 'getopt)
                                         (argv (gensym))
                                         options-variable)
                         &rest args)
                   &body body)
  "Define a \"program\" which mimics a normal program. Useful for small lispy
programs which one wants to be invokable like a normal executable.

--NAME is as in DEFBUILTIN.
--OPTIONS is symbol which parsed options will be bound to.
--FREE is a symbol which free arguments will be bound to.
--SKIP-UNKNOWN-OPTIONS is a form which will be evaluated at runtime. When true
unknown options will be skipped.
--GETOPT is a symbol defining a local option-retrieving function.
--ARGS is a list of arguments which must either be a character, string, or
option specification list. if an argument is a character then a option
specification list is generated with the name being a keyword generated from the
character and the short option being the character itself. If it is a string
then the name will be a keyword generated from the string and the long option is
the string itself.
"
  (let ((outer-args argv)
        (blockname (gensym)))
    (flet ((optparse (option)
             (cond ((characterp option)
                    `(:name ,(intern (string-upcase (coerce (list option) 'string))
                                     (find-package :keyword))
                      :short ,option))
                   ((stringp option)
                    `(:name ,(intern (string-upcase option)
                                     (find-package :keyword))
                      :long ,option))
                   ((listp option) option)
                   (t (error "Dont know how to parse option ~A~%" option)))))
      `(defbuiltin ,(if (listp name)
                        `(,@name :always-in-subshell-p t)
                        (destructuring-bind (s l &rest r)
                            (parse-defbuiltin-name name)
                          (declare (ignore r))
                          `(,s ,l :always-in-subshell-p t)))
           (&rest ,outer-args)
         (handler-case 
             (block ,blockname
               ,(if options-variable
                    `(let ((,options-variable
                             (defopts ,@(mapcar #'optparse args))))
                       (with-options (,options ,free
                                      :argv ,outer-args
                                      :on-error-return-from ,blockname
                                      :skip-unknown-options ,skip-unknown-options
                                      :getopt ,getopt
                                      :options ,options-variable)
                         ,@body))
                    `(with-options (,options ,free
                                    :argv ,outer-args
                                    :on-error-return-from ,blockname
                                    :skip-unknown-options ,skip-unknown-options
                                    :getopt ,getopt
                                    :options (defopts ,@(mapcar #'optparse args)))
                       ,@body)))
           (error () 1))))))

(defbuiltin ("cd" chdir) (&optional dir)
  (handler-case (if dir
                    (uiop:chdir dir)
                    (uiop:chdir (uiop:getenv "HOME")))
    (sb-posix:syscall-error (c)
      (format t "~A: ~A~&" c dir))))

(defbuiltin ("exit" exit-sbcl-shell :eval t) (&optional (code nil cpp))
  (let ((code (if cpp (if code 0 1) 0)))
    (exit :code code)))

(defbuiltin ("source" source-file) (file)
  (load file))

(defbuiltin ("export" sbsh-export) (spec)
  (set-env-variable spec))

(defmain ("repl" sbsh-repl) ((opts free :argv argv
                                        :options-variable defined-options)
                             (:name :help
                              :description "Print this help text"
                              :short #\h
                              :long "help")
                             (:name :exit-token
                              :description "Token used to exit REPL"
                              :short #\e
                              :long "exit-token"
                              :default "EXIT"
                              :arg-parser #'identity
                              :meta-var "TOKEN")
                             (:name :eval
                              :description
                              "a form to evaluate. When provided dont open a repl."
                              :short #\E
                              :long "eval"
                              :default "NIL"
                              :arg-parser #'identity
                              :meta-var "FORM"))
  (declare (ignore free))
  (let ((intty (= 1 (sb-unix:unix-isatty (ensure-stream-fd *standard-input*))))
        (*read-eval* nil))
    (flet ((obtain-input ()
             (when intty
               (format sb-sys:*stdout* "~A> " (package-name *package*))
               (finish-output))
             (read))
           (print-output (str res)
             (unless (string= str "")
               (write-line str)
               (fresh-line))
             (when intty
               (princ res)
               (fresh-line))))
      (cond ((getopt :help)
             (unix-opts:describe
              :prefix "Read and evaluate lisp forms"
              :usage-of "read-eval-lisp"
              :defined-options defined-options))
            ((and (getopt :eval)
                  (not (string= (getopt :eval) "NIL")))
             (let ((e (getopt :eval)))
               (eval (read-from-string e))
               (fresh-line)
               (force-output)))
            (t (let* ((*package* *package*)
                      (exit (read-from-string (getopt :exit-token))))
                 (format t "Starting REPL, exit with the token ~A~&" exit)
                 (loop for sexp = (obtain-input)
                       if (and (atom sexp)
                               (eql sexp exit))
                         do (fresh-line)
                            (return-from sbsh-repl t)
                       else
                         do (let* (res
                                   (str (with-output-to-string (*standard-output*)
                                          (setf res (handler-case (eval sexp)
                                                      (error (c) c))))))
                              (print-output str res)))))))))

(defun apply-alias-transformation (program args)
  (let ((lookup (gethash program *alias-hash*)))
    (if lookup
        (apply lookup program args)
        (cons program args))))

(defun add-alias-transformation (program fn)
  (setf (gethash program *alias-hash*) fn))

(defmacro define-alias-transformation (program (&rest args) &body body)
  `(add-alias-transformation ,program (lambda (,@args) ,@body)))

(defmacro defalias (program expansion)
  (let ((g (gensym)))
    `(define-alias-transformation ,program (&rest ,g)
       (append (list ,@(remove-if (lambda (s) (string= s ""))
                                  (cl-ppcre:split " " expansion)))
               (cdr ,g)))))

(defalias "ls" "ls --color=auto")
