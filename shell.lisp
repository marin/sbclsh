(in-package :sbcl-shell)

(defvar *display-prompt-function* 'default-prompt-with-color
  "Function used to write a prompt, called with an output stream as its only
argument.")

(defvar *ding-on-sigint* nil)

(defun default-prompt-with-color (stream) 
  (let ((cwd (uiop:getcwd)))
    (format stream "[")
    (with-ansi-options (s stream :foreground :green)
      (format s "SBSH"))
    (format stream " ")
    (with-ansi-options (s stream :foreground :magenta)
      (if (string= (butlast-char (namestring cwd)) (uiop:getenv "HOME"))
          (format s "~~")
          (format s "~A" (or (car (last (cl-ppcre:split "/" (namestring cwd))))
                             "/"))))
    (format stream "]$ ")))

(defun run-command (command &rest rest)
  (format t "~&Getting command ~A~%" command)
  (aif (gethash command *builtin-hash*)
       (apply it rest)
       (apply 'piper command rest)))

(defun write-prompt (stream)
  (funcall *display-prompt-function* stream)
  (force-output stream))

(define-alien-callable ("sbsh_ignore_signal" sbsh-ignore-signal)
    (void) ((signo int))
  (declare (ignore signo))
  (values))

(defmacro ignore-signal (signal-number)
  `(alien-funcall (extern-alien "signal" (function void int (function void int)))
                  ,signal-number
                  (alien-callable-function 'sbsh-ignore-signal)))

(defun split-for-pipelining (tokens)
  (let ((pipes nil)
        (curpipe nil))
    (map nil
         (lambda (token)
           (cond ((and (stringp token)
                       (string= token "|"))
                  (push (reverse curpipe) pipes)
                  (setf curpipe nil))
                 ((and (stringp token)
                       (string= token "<"))
                  (push (reverse curpipe) pipes))
                 ((and (stringp token)
                       (string= token ">>"))
                  (push (reverse curpipe) pipes)
                  (setf curpipe (list :append-to-file)))
                 ((and (stringp token)
                       (string= token ">"))
                  (push (reverse curpipe) pipes)
                  (setf curpipe (list :output-to-file)))
                 ((and (null curpipe)
                       (listp token)
                       (keywordp (car token)))
                  (push token pipes))
                 (t
                  (push token curpipe))))
         tokens)
    (push (reverse curpipe) pipes)
    (remove-if #'null (reverse pipes))))

(cffi:defcallback sigint-top-shell-handler :void ((sig :int))
  (declare (ignorable sig))
  (when *ding-on-sigint* 
    (cl-readline:ding))
  (format t "~%")
  (write-prompt *standard-output*))

(defun init-shell ()
  (let ((pgid (sb-posix:getpgrp)))
    (flet ((interactive-shell-p ()
             (sb-unix:unix-isatty (sb-sys:fd-stream-fd sb-sys:*stdin*)))
           (in-foreground-p (pid)
             (= pid (shared-tcgetpgrp (sb-sys:fd-stream-fd sb-sys:*stdin*)))))
      (when (interactive-shell-p)
        (loop for pid = (sb-posix:getpgrp)
              while (not (in-foreground-p pid))
              do (sb-posix:kill (- pgid) sb-posix:sigttin))
        ;; Set up signal handlers
        ;; (macrolet ((ignore-signals (&rest signals)
        ;;              `(progn ,@(mapcar (lambda (s)
        ;;                                  `(ignore-errors (ignore-signal ,s)))
        ;;                                signals))))
        ;;   (ignore-signals sb-posix:sigint
        ;;                   sb-posix:sigquit
        ;;                   sb-posix:sigtstp
        ;;                   sb-posix:sigttin
        ;;                   sb-posix:sigttou
        ;;                   sb-posix:sigchld))
        (set-ignore-signals)
        ;; We need a special handler for sigint, so we use this:
        (cffi:foreign-funcall "signal" :int sb-posix:sigint :pointer
                              (cffi:callback sigint-top-shell-handler))
        (let ((pid (sb-posix:getpid))
              (termios (make-instance 'sb-posix:termios)))
          (ignore-errors
           (when (< 0 (sb-posix:setpgid pid pid))
             (error "Unable to place process in its own process group")))
          (shared-tcsetpgrp (sb-sys:fd-stream-fd sb-sys:*stdin*) pid)
          (sb-posix:tcgetattr (sb-sys:fd-stream-fd sb-sys:*stdin*)
                              termios))
        (setf *debug-stream* sb-sys:*stderr*)
        (set-dispatch-macro-character #\# #\@ '|bang-var-reader|)))))

(defun regular-shell-init ()
  (when (probe-file #P"~/.sbshrc")
    (load #P"~/.sbshrc")))

(defun login-shell-init ()
  (when (probe-file #P"~/.sbsh_profile")
    (load #P"~/.sbsh_profile"))
  (regular-shell-init))

(defun %shell-loop (exit-function)
  (flet ((ex (line)
           (dfmt :shell-loop "~&Line: ~A~&" line)
           (apply 'piper (split-for-pipelining line))))
    (let ((tokens (handler-case (obtain-tokens *standard-input* *standard-output*)
                    (end-of-file () (funcall exit-function)))))
      (dfmt '(:and :shell-loop :tokens) "~&All Tokens: ~A~&" tokens)
      (unwind-protect (when tokens 
                        (loop with pipe = nil
                              for token in tokens
                              if (and (stringp token)
                                      (string= token "&&"))
                                do (let ((exit-code (ex (reverse pipe))))
                                     (if (= exit-code 0)
                                         (setf pipe nil)
                                         (return-from %shell-loop exit-code)))
                              else if (and (stringp token)
                                           (string= token "||"))
                                     do (let ((exit-code (ex (reverse pipe))))
                                          (if (= exit-code 0)
                                              (return-from %shell-loop exit-code)
                                              (setf pipe nil)))
                              else
                                do (push token pipe)
                              finally (when pipe (ex (reverse pipe)))))
        (force-output *standard-output*)))))

(defun shell-loop ()
  (declare (notinline %shell-loop))
  (let ((*package* (find-package :sbcl-shell)))
    (loop (block shloop
            (restart-case 
                (handler-bind ((error
                                 (lambda (c)
                                   (with-ansi-options (s *standard-output*
                                                         :foreground :red)
                                     (format s "Encountered Error: ~S" c))
                                   (format t "~%~A~%" c)
                                   (if *debug-on-error*
                                       (restart-case (error c)
                                         (continue ()
                                           :report "Continue shell session"
                                           (return-from shloop nil))
                                         (exit (&optional (code 1))
                                           :report
                                           (lambda (s)
                                             (if *in-child-process*
                                                 (format s "Exit child process")
                                                 (format s "Exit SBSH")))
                                           (exit :code code)))
                                       (return-from shloop nil)))))
                  (%shell-loop (lambda (&optional return-value)
                                 (format t "exit~%")
                                 (return-from shell-loop return-value))))
              (ignore-error ()
                :report "Ignore error and continue"
                nil))))))

(define-condition malformed-shell-line-error (error)
  ((stream :initarg :stream :reader malformed-shell-line-error-stream)))

(defun script-loop ()
  (labels ((|shell-reader| (s c n)
             (when (null n)
               (setq n 1))
             `(block execblock
                ,@(if (= n 0)
                      (read-until s c n)
                      (loop repeat n 
                            collect (read-shell-in-lisp s c n)))))
           (read-until (s c n)
             (let ((collected nil))
               (do ((line (read-line s) (read-line s)))
                   ((string= line "!")
                    (reverse collected))
                 (push (read-shell-in-lisp s c n line) collected))))
           (read-shell-in-lisp (s c n &optional initial-line)
             (declare (ignore c n))
             (let ((tokens
                     (let ((total-line (or initial-line (read-line s))))
                       (if total-line
                           (labels ((filler ()
                                      (with-input-from-string (local total-line)
                                        (handler-case (or (read-all-tokens local)
                                                          (error 'end-of-file
                                                                 :stream local))
                                          (end-of-file ()
                                            (setf total-line
                                                  (concatenate 'string
                                                               total-line
                                                               (read-line s)))
                                            (filler))))))
                             (filler))
                           (error 'malformed-shell-line-error :stream s))))
                   (collected nil))
               (dfmt '(:and :shell-script :tokens) "~&tokens: ~A~&" tokens)
               (when tokens
                 (loop with pipe = nil
                       for token in tokens
                       if (string= token "&&")
                         do (push `(let ((exit-code
                                           (apply 'piper
                                                  ',(split-for-pipelining
                                                     (reverse pipe)))))
                                     (unless (= exit-code 0)
                                       (return-from execblock nil)))
                                  collected)
                            (setf pipe nil)
                       else if (string= token "||")
                              do (push
                                  `(let ((exit-code
                                           (apply 'piper
                                                  ',(split-for-pipelining
                                                     (reverse pipe)))))
                                     (when (= exit-code 0)
                                       (return-from execblock nil)))
                                  collected)
                                 (setf pipe nil)
                       else
                         do (push token pipe)
                       finally (when pipe
                                 (push `(apply 'piper
                                               ',(split-for-pipelining
                                                  (reverse pipe)))
                                       collected)))
                 `(progn
                    ,@(reverse collected))))))
    (let ((*package* *read-package*))
      (with-open-file (*standard-input* (car (last *posix-argv*)))
        (read-line *standard-input*)
        (let ((disp (get-dispatch-macro-character #\# #\!)))
          (unwind-protect
               (progn
                 (set-dispatch-macro-character #\# #\! #'|shell-reader|)
                 (handler-case (loop (eval (read *standard-input*)))
                   (malformed-shell-line-error (c)
                     (format sb-sys:*stdout*
                             "Malformed shell line when reading from ~A~&Exiting~&"
                             (malformed-shell-line-error-stream c))
                     (exit :code 1))
                   (end-of-file () (exit :code 0))))
            (set-dispatch-macro-character #\# #\! disp)))))))

(defun run-shell ()
  (load-shared-object (find-execute-library))
  (init-shell)
  (cl-readline:register-function :complete #'expander-function)
  (if (char= (char (car *posix-argv*) 0) #\-)
      (login-shell-init)
      (regular-shell-init))
  (let ((final (car (last (cdr *posix-argv*))))
        (pid (sb-posix:getpid)))
    (restart-case (if (and final (probe-file final))
                      (script-loop)
                      (shell-loop))
      (kill-sbsh ()
        :report "Kill SBSH process and all child processes"
        (sb-posix:killpg (sb-posix:getpgid pid) sb-posix:sigkill)))))
